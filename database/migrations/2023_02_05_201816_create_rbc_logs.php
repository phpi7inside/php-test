<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRbcLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rbc_logs', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("method");
            $table->text("url");
            $table->string("code");
            $table->longText("body")->nullable();
            $table->integer("time");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rbc_logs');
    }
}
