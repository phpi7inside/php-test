# Install
php 7.3, screen, mariadb
```sh
# deps
composer i
# setup db
php artisan migrate
php artisan serve
# parser process: screen or pm2
screen -dmS rbc php artisan parse:rbc http://static.feed.rbc.ru/rbc/logical/footer/news.rss
# swagger
php artisan l5-swagger:generate
# open http://127.0.0.1:8000/api/documentation/
```
