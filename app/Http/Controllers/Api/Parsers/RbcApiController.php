<?php

namespace App\Http\Controllers\Api\Parsers;

use App\Http\Controllers\Controller;
use App\Models\Parsers\Rbc;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class RbcController
 * @package App\Http\Controllers\Api\Parsers;
 */
class RbcApiController extends Controller
{
    public function __construct()
    {
    }

    /** @OA\Get(security={{ "bearerAuth": {} }}, tags={"Rbc resource"},
     *     path="/api/rbc", summary="Display a listing of the resource.",
     *     @OA\Parameter(name="page", in="query", description="nullable|integer", example=1,),
     *     @OA\Parameter(name="sort", in="query", description="nullable|in:date-asc,date-desc", example="date-desc",),
     *     @OA\Parameter(name="fields[0]", in="query", description="nullable|array|in:name,desc,published_at,author,img", example="name",),
     *     @OA\Parameter(name="fields[1]", in="query", description="nullable|array|in:name,desc,published_at,author,img", example="desc",),
     *     @OA\Parameter(name="fields[2]", in="query", description="nullable|array|in:name,desc,published_at,author,img", example="published_at",),
     *     @OA\Parameter(name="fields[3]", in="query", description="nullable|array|in:name,desc,published_at,author,img", example="author",),
     *     @OA\Parameter(name="fields[4]", in="query", description="nullable|array|in:name,desc,published_at,author,img", example="img",),
     *     @OA\Response(response=200, description="ok", @OA\MediaType(mediaType="application/json", @OA\Schema(example={"code": "ok"} ))),
     *     @OA\Response(response=422, description="Validate error" ,),
     * )
     * @return LengthAwarePaginator
     */
    public function index()
    {
        $this->authorize("index", Rbc::class);
        $data = validator()->make(request()->all(), [
            "sort" => "nullable|in:date-asc,date-desc",
            "page" => "nullable|integer",
            "fields" => "nullable|array|in:name,desc,published_at,author,img",
        ])->validate();
        $q = Rbc::query();
        if (isset($data["sort"])) {
            $data["sort"] == "date-asc" && $q->orderBy("published_at");
            $data["sort"] == "date-desc" && $q->orderBy("published_at", "desc");
        }
        return $q->paginate(null, $data["fields"]);
    }
}
