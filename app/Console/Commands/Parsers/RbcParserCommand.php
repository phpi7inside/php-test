<?php

namespace App\Console\Commands\Parsers;

use App\Models\Parsers\Rbc;
use App\Models\Parsers\RbcLog;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class RbcParserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "parse:rbc {url}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "parse:rbc http://static.feed.rbc.ru/rbc/logical/footer/news.rss";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $url = $this->argument("url");
        while (true) {
            $timeSleep = 60;
            $timeStart = microtime(true);
            $count = 0;
            try {
                $count = $this->job($url);
            } catch (\Exception $e) {
                Log::error($e);
            }
            $timeWork = microtime(true) - $timeStart;
            echo date("H:i:s") . "  timeWork = $timeWork" . " count = $count \n";
            $timeToSleep = $timeSleep - $timeWork;
            if ($timeToSleep <  0) {
                $timeToSleep = 0;
            }
            usleep($timeToSleep * 10 ** 6);
        }
    }

    private function job($url)
    {
        $timeStart = microtime(true);
        $count = 0;
        $response = Http::get($url);
        $rbc_log = new RbcLog();
        $rbc_log->method = "get"; // hardcode
        $rbc_log->url = $url;
        $rbc_log->code =  $response->status();
        $rbc_log->body =  $response->body();
        $rbc_log->time =  ceil((microtime(true) - $timeStart) * 1000 );
        $rbc_log->save();
        $xml = $response->body();
        $xml_parsed = simplexml_load_string($xml) or die("Error: Cannot create object");
        foreach($xml_parsed->channel->item as $element) {
            if (Rbc::where("guid", (string) $element->guid)->first()) {
                continue;
            }
            $count++;
            $img_path = null;
            if(isset($element->enclosure)) {
                $img_path = $element->enclosure->attributes()->url;
                $ext = pathinfo($img_path, PATHINFO_EXTENSION);
                $local_path = public_path() . "/img/" . Str::random("8") . ".$ext";
                // todo long time operation
                @copy($img_path, $local_path);
                $img_path = str_replace(public_path(), "", $local_path);
            }
            $rbc = new Rbc();
            $rbc->guid = (string) $element->guid;
            $rbc->published_at = Carbon::parse((string) $element->pubDate)->format("c");
            $rbc->desc = (string) $element->description;
            $rbc->name = (string) $element->title;
            $rbc->author = (string) $element->author ?? null;
            $rbc->img = $img_path;
            $rbc->save();
        }
        return $count;
    }
}
