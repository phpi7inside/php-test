<?php

namespace App\Policies\Parsers;

use App\Models\Parsers\Rbc;
use Illuminate\Foundation\Auth\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RbcPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $user = null)
    {
        return true;
    }
}
