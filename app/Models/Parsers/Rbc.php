<?php

namespace App\Models\Parsers;

use Illuminate\Database\Eloquent\Model as ModelBase;

/**
 * @OA\Schema(required={} ,
 *    @OA\Property(property="name", description="name", type="string", example="name"),
 *    @OA\Property(property="desc", description="desc", type="string", example="desc"),
 *    @OA\Property(property="author", description="author", type="string", example="author"),
 *    @OA\Property(property="img", description="img", type="img", example="img"),
 *    @OA\Property(property="published_at", description="published_at", type="string", default="2023-02-02 18:31:45", format="datetime",),
 * ),
 */
class Rbc extends ModelBase
{
    protected $table = "rbc_posts";

    /** @var array */
    protected $guarded = [
        'id',
    ];
    /** @var array */
    protected $with = [
    ];
    /** @var array */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];
    /** @var array */
    protected $appends = [
    ];
    /** @var array */
    protected $casts = [
        "published_at" => "datetime",
    ];

}
