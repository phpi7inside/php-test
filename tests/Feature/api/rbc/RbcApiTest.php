<?php

namespace Tests\Feature\api\rbc;

use App\User as User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Passport;
use App\Models\Parsers\Rbc;
use Tests\TestCase;

/**   @package  */
class RbcApiTest extends TestCase
{
    /**  @return void */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**  @return void */
    public function testIndex()
    {
        $data = [
            "page" => 2,
            "fields" => ["img", "published_at"],
        ];
        $response = $this->json('get', '/api/rbc', $data);
        $response->assertStatus(200);
        $response->assertJsonStructure(['data']);
    }

    /**  @return void */
    public function test422()
    {
        $data = [
            "sort" => "-",
        ];
        $response = $this->json('get', '/api/rbc', $data);
        $response->assertStatus(422);
    }


    /**  @return void */
    public function testFilters()
    {
        (new Rbc(["name" => "name", "desc" => "desc", "guid" => "qwe", "published_at" => Carbon::parse("2020-11-22")]))->save();
        (new Rbc(["name" => "name", "desc" => "desc", "guid" => "qwe", "published_at" => Carbon::parse("2020-11-23")]))->save();
        (new Rbc(["name" => "name", "desc" => "desc", "guid" => "qwe", "published_at" => Carbon::parse("2020-11-24")]))->save();
        //
        $data = [
            "fields" => ["published_at", "name"],
            "sort" => "date-desc",
        ];
        $response = $this->json('get', '/api/rbc', $data);
        $response->assertStatus(200);
        $response->assertJsonStructure(["data" => ["*" => ["published_at", "name"]]]);
        $response->assertJsonMissing(["data" => ["*" => ["desc", "id", "guid"]]]);
        $this->assertEquals("2020-11-24", Carbon::parse($response->getData()->data[0]->published_at)->format("Y-m-d"));
        //
        $data = [
            "fields" => ["published_at"],
            "sort" => "date-asc",
        ];
        $response = $this->json('get', '/api/rbc', $data);
        $response->assertStatus(200);
        $this->assertEquals("2020-11-22", Carbon::parse($response->getData()->data[0]->published_at)->format("Y-m-d"));
    }
}
